Designing a basic compiler for a greenfield programming language in Python. 
Created a Lexer, which produces tokens which are used for analysing the code, with the help of Parser created. 
Working upon building basic programming language features like strings, variables and if statements. 
More features will be implemented as we advance further in the project.
